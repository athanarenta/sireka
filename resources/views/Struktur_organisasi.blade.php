<body class="antialiased container">
    <section>
        <div class="text-center mb-1 mt-5">
            <h6> <strong>
                    Struktur Organisasi
                </strong></h6>
            <hr color="red" class="col-md-1 mt-1">
        </div>
        <div class="mt-5 px-4 py-2 text-center">
            <div class="mx-auto d-block text-center text-white" style="height:180px;border-radius:10px;">
                <img src="{{ asset('img/image 6.png') }}" style="width:180px;height:180px;border-radius:10px;">
            </div>
            <h5 class="mt-3"><strong>Isabela</strong></h5>
            <h6>Direktur</h6>
        </div>
        <div class="row">
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 15.png') }}" style="width:180px;height:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Jhon</strong></h5>
                <h6>Sekretaris I</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 17.png') }}" style="width:180px;height:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Raphael</strong></h5>
                <h6>Sekretaris II</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-10.png') }}" style="height:180px;width:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Bethany</strong></h5>
                <h6>Bendahara I</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-9.png') }}" style="width:180px;height:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Mikel Kane</strong></h5>
                <h6>Bendahara II</h6>
            </div>
        </div>
        <div class="text-center">
            <h5 class="mt-3 mb-3"><strong>Dokter</strong></h5>
        </div>
        <div class="row">
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 5.png') }}" style="width:180px;height:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Gigis</strong></h5>
                <h6>Dokter Gigi</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 7.png') }}" style="width:180px;height:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Sasa</strong></h5>
                <h6>Dokter Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 8.png') }}" style="height:180px;width:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Bekti</strong></h5>
                <h6>Dokter Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class="text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-10.png') }}" style="width:180px;height:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Alianova</strong></h5>
                <h6>Dokter Kandungan</h6>
            </div>
        </div>
        <div class="text-center">
            <h5 class="mt-3 mb-3"><strong>Perawat</strong></h5>
        </div>
        <div class="row">
            <div class="col-md-3 mb-5 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 5.png') }}" style="width:180px;height:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Robert</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 7.png') }}" style="width:180px;height:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Lia</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 8.png') }}" style="height:180px;width:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Rafel</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class="text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-10.png') }}" style="width:180px;height:180px;border-radius:10px;">
                </div>
                <h5 class="mt-3"><strong>Restia</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
        </div>
    </section>
</body>