<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>AdminLTE 3 | Dashboard 2</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css?v=').time() }}">
  <!-- Google Font: Source Sans Pro -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;800&display=swap" rel="stylesheet">
	<style type="text/css">
		.bg-overlay{
			background: linear-gradient(rgba(255,0,0,.7), rgba(255,0,0,.7)), url('http://127.0.0.1:8000/img/image 19.png');
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center center;
			width: 100%;
			height: 100%;
		}
	</style>
	<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>

</head>
<body style="overflow-x: hidden;">
	<section class="">
		<nav class="navbar navbar-expand-lg navbar-light fixed-top position-absolute" id="navbarScroll" style="background: white; padding: 1em 7%">
			<a href="#" class="navbar-brand">
				<img src="{{ URL::asset('img/Pmi.png')}}" width="45" style="margin-top: -7px ">
				<span class="mt-3">
					<h5 class="font-weight-normal d-inline-block mx-3" style="margin-top: 7px;"> Palang Merah Indonesia </h5>
				</span>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarToggler">
				<ul class="navbar-nav ml-auto my-auto mt-lg-0">
					<li class="nav-item pt-1
					@if (\Request::is('/'))  
					  active
					@endif">
						<a href="{{ URL('/') }}" class="nav-link mt-1">Beranda</a>
					</li>
					<li class="nav-item pt-1">
						<a href="#" class="nav-link mt-1">Tentang Kami</a>
					</li>
					<li class="nav-item pt-1">
						<a href="#" class="nav-link mt-1">Buat Janji</a>
					</li>
					<li class="nav-item pt-1">
						<a href="#" class="nav-link mt-1">Cek Covid-19</a>
					</li>
					<li class="nav-item pt-1 d-md-none d-lg-inline-block">
						<a class="nav-link mt-1">|</a>
					</li>
					<li class="nav-item pt-1">
						<a href="#" class="nav-link mt-1">Masuk</a>
					</li>
					<li class="nav-item">
						<a href="#" class="nav-link">
							<button class="btn" style="background: transparent; border-color: red; color: red;margin-top:2px;/*box-shadow: 0px 0px 5px 0px rgba(255,0,0,0.75);*/">
								Daftar
							</button>
						</a>
					</li>
				</ul>
			</div>
		</nav>
	</section>
	<section class="">
		<div class="mb-5" style="margin: 0 7%;padding-top: 10%">
			<div class="text-center">
				<h3>Tentang Kami</h3>
				<hr class="bg-danger mt-0" width="3%">
			</div>
			<div class="row" style="">
				<div class="col-md-5">
					<div class="card text-center shadow-none">
						<img src="http://127.0.0.1:8000/img/Rectangle 43.png" class="card-img">
					</div>
				</div>
				<div class="col-md-7" style="padding-right: 0">
					<div class="">
						<h4>Visi</h4>
						<h5 class="text-uppercase">"terwujudnya pmi yang profesional dan berintegritas serta bergerak bersama masyarakat"</h5>
						<h4 class="mt-4">Misi</h4>
						<h5 class="">
							<ol>
								<li>Memelihara reputasi organisasi PMI di tingkat nasional dan internasional.</li>
								<li>Menjadi organisasi kemanusiaan terdepan yang memberikan layanan berkualitas kepada masyarakat sesuai dengan Prinsip-Prinsip Dasar Gerakan Internasional Palang Merah dan Bulan Sabit Merah.</li>
							</ol>
						</h5>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="">
		<div class="mb-5" style="margin: 0 7%;padding-top: 10%">
			<div class="text-center">
				<h3>Layanan Kami</h3>
				<hr class="bg-danger mt-0" width="3%">
			</div>
			<div class="row" style="">
				<div class="col-md-4 p-5">
					<div class="card">
						<div class="card-body text-center shadow" style="min-height: 360px;">
							<img src="{{ asset('img/Sedot-darah.png') }}" class="mb-3">
							<h4>Sedot Darah</h4>
							<p class="small px-5">Donor darah adalah orang yang memberikan darah secara sukarela untuk maksud dan tujuan transfusi darah bagi orang lain yang membutuhkan</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 p-5">
					<div class="card">
						<div class="card-body text-center shadow" style="min-height: 360px;">
							<img src="{{ asset('img/Flat.png') }}" class="mb-3">
							<h4>Medical Checkup</h4>
							<p class="small px-5">Medical check up merupakan pemeriksaan kesehatan secara menyeluruh guna memastikan kondisi kesehatan</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 p-5">
					<div class="card">
						<div class="card-body text-center shadow" style="min-height: 360px;">
							<img src="{{ asset('img/charity 1.png') }}" class="mb-3">
							<h4>Relawan bencana</h4>
							<p class="small px-5">Relawan bencana adalah seseorang atau sekelompok orang yang memiliki kemampuan dan kepedulian secara sukarela untuk membantu sesama</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="" style="margin: 0 7%">
        <div class="text-center mb-1 mt-5">
            <h3>
                Struktur Organisasi
            </h3>
            <hr color="red" class="mt-0" width="3%">
        </div>
        <div class="mt-5 px-4 py-2 text-center">
            <div class="mx-auto d-block text-center text-white " style="height:180px;border-radius:10px;">
                <img src="{{ asset('img/image 6.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
            </div>
            <h5 class="mt-3"><strong>Isabela</strong></h5>
            <h6>Direktur</h6>
        </div>
        <div class="row">
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 15.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Jhon</strong></h5>
                <h6>Sekretaris I</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 17.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Raphael</strong></h5>
                <h6>Sekretaris II</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-10.png') }}"style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Bethany</strong></h5>
                <h6>Bendahara I</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-9.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Mikel Kane</strong></h5>
                <h6>Bendahara II</h6>
            </div>
        </div>
        <div class="text-center">
            <h5 class="mt-3 mb-3"><strong>Dokter</strong></h5>
        </div>
        <div class="row">
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 5.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Gigis</strong></h5>
                <h6>Dokter Gigi</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 7.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Sasa</strong></h5>
                <h6>Dokter Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 8.png') }}"style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Bekti</strong></h5>
                <h6>Dokter Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class="text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-10.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Alianova</strong></h5>
                <h6>Dokter Kandungan</h6>
            </div>
        </div>
        <div class="text-center">
            <h5 class="mt-3 mb-3"><strong>Perawat</strong></h5>
        </div>
        <div class="row">
            <div class="col-md-3 mb-5 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 5.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Robert</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 7.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Lia</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 8.png') }}"style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Rafel</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class="text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-10.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Restia</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
        </div>
    </section>
	<section class="min-vh-100">
		<div class="mb-5" style="margin-left: 7%;padding-top: 10%">
			<div class="row" style="">
				<div class="col-md-5 my-5">
					<h1 class="font-weight-bold" style="line-height: 72px">Setetes Darah Anda, Nyawa Bagi Sesama</h1>
					<h6 class="my-3 py-4 font-weight-normal">Menolong sesama tanpa membeda-bedakan</h6>
					<a href="#">
						<button class="btn btn-danger px-4 py-2 small" style="box-shadow: 0px 0px 5px 0px rgba(255,0,0,0.75);">Buat Janji</button>
					</a>
				</div>
				<div class="col-md-7" style="padding-right: 0">
					<div class="shadow-none">
						<div class="bg-danger" style="border-radius: 10% 0 0 10%;padding-bottom: 65%">
							<img src="{{ URL::asset('img/bofu-shaw-4TjWvcKElJA-unsplash-1.png') }}" style="position:absolute;width: 100%;top: 0; right: 0;height: 100%;border-radius: 10% 0 0 0;">
							<span style="position:absolute;width: 100%;top: 0; right: 0;height: 100%;background:rgba(180,30,30,0.8);border-radius: 10% 0 0 0;"></span>
							<img src="{{ URL::asset('img/pexels-thirdman-5327585-removebg-preview 1.png') }}" width="45%" style="left:0;bottom: 0;position: absolute;margin-left: -80px;">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="min-vh-100">
		<div class="" style="padding: 0 7%">
			<h2 class="font-weight-normal text-center py-5">Cek Mandiri COVID-19</h2>
			<div class="row">
				<div class="col-md-6">
					<div class="card text-center shadow-none">
						<img src="http://127.0.0.1:8000/img/image 19.png" width="539px" height="404px" class="card-img" style="border-radius: 3%">
						<div class="card-img-overlay" style="background: rgba(180,30,30,0.2);border-radius: 3%"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="py-5 pl-5">
						<p class="text-uppercase mb-1 small" style="font-size: 12px;">cek mandiri covid-19</p>
						<h2 class="font-weight-bold">Khawatir  akan COVID-19? Yuk cek risiko kamu di sini!</h2>
						<p class="small" style="font-size: 12px;">PMI BANYUMAS menyediakan tes online untuk membantu skrining awal dalam mendeteksi COVID-19 di masyarakat. Tes online ini berisi dari beberapa pertanyaan mengenai gejala atau risiko penularan COVID-19.</p>
						<a href="#" class="">
							<button class="btn btn-danger px-4 py-2 my-3" style="box-shadow: 0px 0px 5px 0px rgba(255,0,0,0.75);">Cek Sekarang</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	

	<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ URL::asset('lte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ URL::asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ URL::asset('lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('lte/dist/js/adminlte.js') }}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{ URL::asset('lte/dist/js/demo.js') }}"></script>

<!-- PAGE PLUGINS -->
<!-- ChartJS -->
<script src="{{ URL::asset('lte/plugins/chart.js/Chart.min.js') }}"></script>

<!-- PAGE SCRIPTS -->
<script src="{{ URL::asset('lte/dist/js/pages/dashboard2.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(window).scroll(function () {
			if ($(this).scrollTop()>50) {
				$('#navbarScroll').css('background-color','white');
			}else{
				$('#navbarScroll').css('background-color','black');
			}
		}
	});
</script>
</body>
</html>