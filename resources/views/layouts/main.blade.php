<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>@yield('Title')</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css?v=').time() }}">
  <!-- Google Font: Source Sans Pro -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;800&display=swap" rel="stylesheet">
	<style type="text/css">
		.bg-overlay{
			background: linear-gradient(rgba(255,0,0,.7), rgba(255,0,0,.7)), url('http://127.0.0.1:8000/img/image 19.png');
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center center;
			width: 100%;
			height: 100%;
		}
	</style>
	<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>

</head>
<body style="overflow-x: hidden;">
	<section class="">
		<nav class="navbar navbar-expand-lg navbar-light fixed-top position-absolute" id="navbarScroll" style="background: white; padding: 1em 7%">
			<a href="#" class="navbar-brand">
				<img src="{{ URL::asset('img/Pmi.png')}}" width="45" style="margin-top: -7px ">
				<span class="mt-3">
					<h5 class="font-weight-normal d-inline-block mx-3" style="margin-top: 7px;"> Palang Merah Indonesia </h5>
				</span>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarToggler">
				<ul class="navbar-nav ml-auto my-auto mt-lg-0">
					<li class="nav-item pt-1
					@if (\Request::is('/'))  
					  active
					@endif">
						<a href="{{ URL('/') }}" class="nav-link mt-1">Beranda</a>
					</li>
					<li class="nav-item pt-1
					@if (\Request::is('tentang-kami'))  
					  active
					@endif">
						<a href="{{ URL('tentang-kami') }}" class="nav-link mt-1">Tentang Kami</a>
					</li>
					<li class="nav-item pt-1
					@if (\Request::is('/buat-janji'))  
					  active
					@endif">
						<a href="" class="nav-link mt-1">Buat Janji</a>
					</li>
					<li class="nav-item pt-1">
						<a href="#" class="nav-link mt-1">Cek Covid-19</a>
					</li>
					<li class="nav-item pt-1 d-md-none d-lg-inline-block">
						<a class="nav-link mt-1">|</a>
					</li>
					<li class="nav-item pt-1">
						<a href="#" class="nav-link mt-1">Masuk</a>
					</li>
					<li class="nav-item">
						<a href="#" class="nav-link">
							<button class="btn" style="background: transparent; border-color: red; color: red;margin-top:2px;/*box-shadow: 0px 0px 5px 0px rgba(255,0,0,0.75);*/">
								Daftar
							</button>
						</a>
					</li>
				</ul>
			</div>
		</nav>
	</section>
	@yield('BawahNavbar')
	@yield('Content')
	

	<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ URL::asset('lte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ URL::asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ URL::asset('lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('lte/dist/js/adminlte.js') }}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{ URL::asset('lte/dist/js/demo.js') }}"></script>

<!-- PAGE PLUGINS -->
<!-- ChartJS -->
<script src="{{ URL::asset('lte/plugins/chart.js/Chart.min.js') }}"></script>

<!-- PAGE SCRIPTS -->
<script src="{{ URL::asset('lte/dist/js/pages/dashboard2.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(window).scroll(function () {
			if ($(this).scrollTop()>50) {
				$('#navbarScroll').css('background-color','white');
			}else{
				$('#navbarScroll').css('background-color','black');
			}
		}
	});
</script>
</body>
</html>