@extends('layouts.main')

@section('Title','Tentang Kami')

@section('BawahNavbar')
	<section class="">
		<div class="mb-5" style="margin: 0 7%;padding-top: 10%">
			<div class="text-center">
				<h3>Tentang Kami</h3>
				<hr class="bg-danger mt-0" width="3%">
			</div>
			<div class="row" style="">
				<div class="col-md-5">
					<div class="card text-center shadow-none">
						<img src="http://127.0.0.1:8000/img/Rectangle 43.png" class="card-img">
					</div>
				</div>
				<div class="col-md-7" style="padding-right: 0">
					<div class="">
						<h4>Visi</h4>
						<h5 class="text-uppercase">"terwujudnya pmi yang profesional dan berintegritas serta bergerak bersama masyarakat"</h5>
						<h4 class="mt-4">Misi</h4>
						<h5 class="">
							<ol>
								<li>Memelihara reputasi organisasi PMI di tingkat nasional dan internasional.</li>
								<li>Menjadi organisasi kemanusiaan terdepan yang memberikan layanan berkualitas kepada masyarakat sesuai dengan Prinsip-Prinsip Dasar Gerakan Internasional Palang Merah dan Bulan Sabit Merah.</li>
							</ol>
						</h5>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('Content')
<section class="">
		<div class="mb-5" style="margin: 0 7%;padding-top: 10%">
			<div class="text-center">
				<h3>Layanan Kami</h3>
				<hr class="bg-danger mt-0" width="3%">
			</div>
			<div class="row" style="">
				<div class="col-md-4 p-5">
					<div class="card">
						<div class="card-body text-center shadow" style="min-height: 360px;">
							<img src="{{ asset('img/Sedot-darah.png') }}" class="mb-3">
							<h4>Sedot Darah</h4>
							<p class="small px-5">Donor darah adalah orang yang memberikan darah secara sukarela untuk maksud dan tujuan transfusi darah bagi orang lain yang membutuhkan</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 p-5">
					<div class="card">
						<div class="card-body text-center shadow" style="min-height: 360px;">
							<img src="{{ asset('img/Flat.png') }}" class="mb-3">
							<h4>Medical Checkup</h4>
							<p class="small px-5">Medical check up merupakan pemeriksaan kesehatan secara menyeluruh guna memastikan kondisi kesehatan</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 p-5">
					<div class="card">
						<div class="card-body text-center shadow" style="min-height: 360px;">
							<img src="{{ asset('img/charity 1.png') }}" class="mb-3">
							<h4>Relawan bencana</h4>
							<p class="small px-5">Relawan bencana adalah seseorang atau sekelompok orang yang memiliki kemampuan dan kepedulian secara sukarela untuk membantu sesama</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="" style="margin: 0 7%">
        <div class="text-center mb-1 mt-5">
            <h3>
                Struktur Organisasi
            </h3>
            <hr color="red" class="mt-0" width="3%">
        </div>
        <div class="mt-5 px-4 py-2 text-center">
            <div class="mx-auto d-block text-center text-white " style="height:180px;border-radius:10px;">
                <img src="{{ asset('img/image 6.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
            </div>
            <h5 class="mt-3"><strong>Isabela</strong></h5>
            <h6>Direktur</h6>
        </div>
        <div class="row">
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 15.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Jhon</strong></h5>
                <h6>Sekretaris I</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 17.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Raphael</strong></h5>
                <h6>Sekretaris II</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-10.png') }}"style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Bethany</strong></h5>
                <h6>Bendahara I</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-9.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Mikel Kane</strong></h5>
                <h6>Bendahara II</h6>
            </div>
        </div>
        <div class="text-center">
            <h5 class="mt-3 mb-3"><strong>Dokter</strong></h5>
        </div>
        <div class="row">
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 5.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Gigis</strong></h5>
                <h6>Dokter Gigi</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 7.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Sasa</strong></h5>
                <h6>Dokter Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 8.png') }}"style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Bekti</strong></h5>
                <h6>Dokter Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class="text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-10.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Alianova</strong></h5>
                <h6>Dokter Kandungan</h6>
            </div>
        </div>
        <div class="text-center">
            <h5 class="mt-3 mb-3"><strong>Perawat</strong></h5>
        </div>
        <div class="row">
            <div class="col-md-3 mb-5 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 5.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Robert</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 7.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Lia</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class=" text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image 8.png') }}"style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Rafel</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
            <div class="col-md-3 px-4 py-2 text-center">
                <div class="text-center text-white " style="height:180px;border-radius:10px;">
                    <img src="{{ asset('img/image-10.png') }}" style="width:180px;height:180px;border-radius:10px;" class="shadow">
                </div>
                <h5 class="mt-3"><strong>Restia</strong></h5>
                <h6>Perawat Umum</h6>
            </div>
        </div>
    </section>
@endsection