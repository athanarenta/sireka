@extends('layouts.main')

@section('Title','Palang Merah Indonesia')

@section('BawahNavbar')
	<section class="min-vh-100">
		<div class="mb-5" style="margin-left: 7%;padding-top: 10%">
			<div class="row" style="">
				<div class="col-md-5 my-5">
					<h1 class="font-weight-bold" style="line-height: 72px">Setetes Darah Anda, Nyawa Bagi Sesama</h1>
					<h6 class="my-3 py-4 font-weight-normal">Menolong sesama tanpa membeda-bedakan</h6>
					<a href="#">
						<button class="btn btn-danger px-4 py-2 small" style="box-shadow: 0px 0px 5px 0px rgba(255,0,0,0.75);">Buat Janji</button>
					</a>
				</div>
				<div class="col-md-7" style="padding-right: 0">
					<div class="shadow-none">
						<div class="bg-danger" style="border-radius: 10% 0 0 10%;padding-bottom: 65%">
							<img src="{{ URL::asset('img/bofu-shaw-4TjWvcKElJA-unsplash-1.png') }}" style="position:absolute;width: 100%;top: 0; right: 0;height: 100%;border-radius: 10% 0 0 0;">
							<span style="position:absolute;width: 100%;top: 0; right: 0;height: 100%;background:rgba(180,30,30,0.8);border-radius: 10% 0 0 0;"></span>
							<img src="{{ URL::asset('img/pexels-thirdman-5327585-removebg-preview 1.png') }}" width="45%" style="left:0;bottom: 0;position: absolute;margin-left: -80px;">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('Content')
    <section class="py-5">
        <div class="mt-5" style="padding: 0 7%">
            <div class="row">
                <div class="col-md-6">
                    <div class="" style="min-width: 100%;min-height:70vh">
                        <img src="{{ asset('img/doctor.jpg') }}"
                            style="width: 60%; border-radius: 20px; transform: scaleX(-1)">
                        <div class="card position-absolute" style="width: 60%; bottom: 0; right:0; border-radius: 20px">
                            <img src="{{ asset('img/logo.png') }}" class="position-absolute"style="right: 0; top: 0;margin: -80px -80px 0 0">
                            <div class="card-body bg-danger" style="border-radius: 20px">
                                <br><br>
                                <h4>Jam Buka</h4>
                                <div class="row">
                                	<p class="col-lg-3 col-md-4 col-3">Senin</p>
	                                <hr class="col-lg-2 col-md-1 col-2" color="white">
	                                <p class="col-6">08.00 - 20.00</p>
                                </div>
                                <div class="row">
                                	<p class="col-lg-3 col-md-4 col-3">Selasa</p>
	                                <hr class="col-lg-2 col-md-1 col-2" color="white">
	                                <p class="col-6">08.00 - 20.00</p>
                                </div>
                                <div class="row">
                                	<p class="col-lg-3 col-md-4 col-3">Rabu</p>
	                                <hr class="col-lg-2 col-md-1 col-2" color="white">
	                                <p class="col-6">08.00 - 20.00</p>
                                </div>
                                <div class="row">
                                	<p class="col-lg-3 col-md-4 col-3">Kamis</p>
	                                <hr class="col-lg-2 col-md-1 col-2" color="white">
	                                <p class="col-6">08.00 - 20.00</p>
                                </div>
                                <div class="row">
                                	<p class="col-lg-3 col-md-4 col-3">Jumat</p>
	                                <hr class="col-lg-2 col-md-1 col-2" color="white">
	                                <p class="col-6">08.00 - 20.00</p>
                                </div>
                                <div class="row">
                                	<p class="col-lg-3 col-md-4 col-3">Sabtu</p>
	                                <hr class="col-lg-2 col-md-1 col-2" color="white">
	                                <p class="col-6">08.00 - 20.00</p>
                                </div>
                                <div class="row">
                                	<p class="col-lg-3 col-md-4 col-3">Minggu</p>
	                                <hr class="col-lg-2 col-md-1 col-2" color="white">
	                                <p class="col-6">08.00 - 20.00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card-body align-items-start">
                        <p>TENTANG KAMI -</p>
                        <h2 class="font-weight-bold">Profesional dalam Tugas <br> Melayani dengan Hati</h2>
                        <p style="text-align: justify">Kepada seluruh tenaga kesehatan, tumbuhkan jiwa melayani
                            dalam sanubari dan berikan pelayanan sepenuh hati. Tenaga kesehatan harus bangga untuk
                            melayani, memberikan yang terbaik dan melindungi masyarakat.</p>
                        <p class="mb-5" style="text-align: justify">Palang Merah Indonesia (PMI) adalah sebuah
                            organisasi perhimpunan nasional di Indonesia yang bergerak dalam bidang sosial
                            kemanusiaan.</p>
                        <a href="#" style="color: red">Lebih Lanjut ></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="padding: 0 7%">
    	<div class="text-center mb-4 mt-3">
	        <h6>- PELAYANAN KAMI -</h6>
	        <h2>Layanan Beritas Terpercaya</h2>
	        <h6>PMI selalu mempunyai Tujuh Prinsip Dasar Gerakan Internasional Palang Merah dan Bulan Sabit Merah</h6>
	        <h6> yaitu Kesamaan, Kesukarelaan, Kemandirian, Kesatuan, Kenetralan dan Kesemestaan</h6>

	    </div>
	    <div class="row">
	        <br>
	        <div class="col-md-3 px-4 py-2">
	            <div class="px-3 text-center text-white " style="height:180px;border-radius:10px;">
	                <img src="{{ asset('img/image 5.png') }}" style="width:180px;height:180px;border-radius:10px;">
	            </div>
	            <br>
	        </div>
	        <div class="col-md-3 px-4 py-2">
	            <div class="px-3 text-center text-white " style="height:180px;border-radius:10px;">
	                <img src="{{ asset('img/image 6.png') }}" style="width:180px;height:180px;border-radius:10px;">
	            </div>
	            <br>
	        </div>
	        <div class="col-md-3 px-4 py-2">
	            <div class="px-3 text-center text-white " style="height:180px;border-radius:10px;">
	                <img src="{{ asset('img/image 7.png') }}" style="width:180px;height:180px;border-radius:10px;">
	            </div>
	            <br>
	        </div>
	        <div class="col-md-3 px-4 py-2">
	            <div class="px-3 text-center text-white " style="height:180px;border-radius:10px;">
	                <img src="{{ asset('img/image 8.png') }}" style="width:180px;height:180px;border-radius:10px;">
	            </div>
	            <br>
	        </div>
	    </div>
	    <div class="row mb-1">
	        <br>
	        <div class="col-md-3 px-4 py-2">
	            <div class=" text-center text-white " style="height:180px;border-radius:10px;">
	                <img src="{{ asset('img/image 15.png') }}" style="width:180px;height:180px;border-radius:10px;">
	            </div>
	            <br>
	        </div>
	        <div class="col-md-3 px-4 py-2">
	            <div class=" text-center text-white " style="height:180px;border-radius:10px;">
	                <img src="{{ asset('img/image 17.png') }}" style="width:180px;height:180px;border-radius:10px;">
	            </div>
	            <br>
	        </div>
	        <div class="col-md-3 px-4 py-2">
	            <div class=" text-center text-white " style="height:180px;border-radius:10px;">
	                <img src="{{ asset('img/image-9.png') }}" style="width:180px;height:180px;border-radius:10px;">
	            </div>
	            <br>
	        </div>
	        <div class="col-md-3 px-4 py-2">
	            <div class=" text-center text-white " style="border-radius:10px;">
	                <img src="{{ asset('img/image-10.png') }}" style="width:180px;border-radius:10px;">
	            </div>
	            <br>
	        </div>
	        <div class="col-12 mt-3">
	            <div class="text-center">
	                <a href="#" class="text-center">
	                    <button class='btn btn-danger font-weight-light mb-5'
	                        style="box-shadow: 0px 0px 5px 0px rgba(255,0,0,0.75)">
	                        Lebih Banyak</button>
	                </a>
	            </div>
	        </div>
	    </div>
	</section>
	<section>
	    <div class="text-center" style="padding-right: 0">
	        <div class="card" style="">
	            <div class="bg-danger">
	                <span class="py-4 fa fa-phone-alt fa-2x"></span>
	                <h3 class="font-weight-normal">Panggilan Gawat Darurat</h3>
	                <button class="btn btn-dark font-weight-light mt-3 mb-4"
	                    style="box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75)">
	                    <span class="iconify d-inline-block mr-2" data-icon="feather-phone-call" data-inline="false"></span>
	                    (021) 5671234
	                </button>
	            </div>
	        </div>
	    </div>
    </section>
	<section class="min-vh-100">
		<div class="" style="padding: 0 7%">
			<h2 class="font-weight-normal text-center py-5">Cek Mandiri COVID-19</h2>
			<div class="row">
				<div class="col-md-6">
					<div class="card text-center shadow-none">
						<img src="http://127.0.0.1:8000/img/image 19.png" width="400px" height="400px" class="card-img" style="border-radius: 3%;clip-path: polygon(25% 0, 100% 0, 100% 100%, 13% 50%);">
						<div class="card-img-overlay" style="background: rgba(180,30,30,0.2);border-radius: 3%;clip-path: polygon(25% 0, 100% 0, 100% 100%, 13% 50%);"></div>
						<div width="539px" height="404px" class="bg-danger position-absolute" style="min-width: 539px; min-height: 404px;bottom: 0;left: 0;clip-path: polygon(23% 3%, 10% 53%, 100% 97%, 3% 57%);"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="py-5 pl-5">
						<p class="text-uppercase mb-1 small" style="font-size: 12px;">cek mandiri covid-19</p>
						<h2 class="font-weight-bold">Khawatir  akan COVID-19? Yuk cek risiko kamu di sini!</h2>
						<p class="small" style="font-size: 12px;">PMI BANYUMAS menyediakan tes online untuk membantu skrining awal dalam mendeteksi COVID-19 di masyarakat. Tes online ini berisi dari beberapa pertanyaan mengenai gejala atau risiko penularan COVID-19.</p>
						<a href="#" class="">
							<button class="btn btn-danger px-4 py-2 my-3" style="box-shadow: 0px 0px 5px 0px rgba(255,0,0,0.75);">Cek Sekarang</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection